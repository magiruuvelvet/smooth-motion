QT += core
QT -= gui

CONFIG += c++14

TARGET = smooth-motion
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    avutil/AVUtil.cpp \
    avutil/AVInfo.cpp

HEADERS += \
    avutil/AVUtil.hpp \
    avutil/AVInfo.hpp

# use pkg-config to import dependencies
CONFIG += link_pkgconfig
PKGCONFIG += zlib opencv libswresample libavformat libavutil libavcodec
#>>> use this if above code doesn't work for you
#LIBS += `pkg-config --libs zlib opencv libswresample libavformat libavutil libavcodec`
#INCLUDEPATH += `pkg-config --cflags zlib opencv libswresample libavformat libavutil libavcodec`

# Looking forward to Qt 6 :)
DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
