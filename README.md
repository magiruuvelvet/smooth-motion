# **smooth motion**

*smooth motion* is a motion interpolation tool for videos which renders intermediate frames based on motion to increase a video's frame rate. smooth motion is a fork of **[butterflow](https://github.com/dthpham/butterflow)** for more professional use cases and defaults to **lossless** - lossless input (highly recommended) -> lossless output. smooth motion works with `ffvhuff` and `ffv1` to compress the output without using lossless encoders like x264 or x265 in "lossless mode". Optimally it can also write directly to uncompressed RAW without compressing it.

LOSSY OUTPUT IS **NOT** SUPPORTED!!!

## Features

Same as **butterflow**.

 - Makes **motion interpolated videos** (increase a video's frame rate by rendering intermediate frames based on motion, uses a combination of pixel-warping and blending).
 - Makes **smooth motion videos** (simple blending between frames).
 - Leverages interpolated frames to make **fluid slow motion videos**.

<br>

 - Accepts *any video codec* as input which your local ffmpeg build supports (decoding only).

Only in **smooth motion**

 - 10/12/14/16-bit support (butterflow only does 8-bit)
 - YUV 4:2:0, 4:2:2, 4:4:4 support (butterflow only does 4:2:0)
 - RGB color space output (alternative to YUV) (again, butterflow only writes YUV videos)
   - Side note: smooth motion (as well as butterflow) uses RGB interally and converts to YUV later on. if you need excelent looking colors (for archiving purposes) save to RGB and convert to YUV later on during the encoding process.

Please note: this tools is aimed ***only*** for videos and therefore ignores all audio tracks. You need to repack your videos manually after the interpolation is finished. This is intended behavior!

## Requirements

 - 64-bit x86 system (!)
 - `ffmpeg` with support for `ffvhuff` and `ffv1` compression codec, and the `nut` multimedia container format. Check with `ffmpeg -codecs | egrep 'ffvhuff|ffv1` and `ffmpeg -muxers | grep nut` (note: uninstall `libnut` if you see it, its broken. only ffmpeg's builtin nut muxer is supported)
 - **OpenCV** 2.4 <sub>for now, butterflow uses it</sub> (I will try to port this over to OpenCV 3)
 - A strong and modern GPU if you don't want to wait years for a video to complete `:^)` <br> Note: If you have NVIDIA use CUDA for a improved rendering performance. (optimal)
 - Plenty of free disk space (if you ever worked with RAW videos you know why, if not expect about 10~30GB for a ~4 minute video depending on the pixel format used, yuv vs rgb and the bit depth)


## ...

(placeholder for futher content when the tool reaches a useable state)
