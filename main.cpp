#include <QCoreApplication>

#include <iostream>
#include <avutil/AVUtil.hpp>

int main(int argc, char **argv)
{
    QCoreApplication a(argc, argv);

    AVUtil test("");
    if (!test)
    {
        std::cerr << "Uh oh, no such video file or other error :(" << std::endl;
        return 1;
    }

    std::cout << qUtf8Printable(test.get_av_info_as_text()) << std::endl;

    return 0;
}
