#ifndef AVINFO_HPP
#define AVINFO_HPP

#include <QtGlobal>
#include <QTime>

struct AVInfo
{
    bool has_video;   // most important one

    quint32 width, height;
    quint32 sar_n, sar_d;
    quint32 dar_n, dar_d;

    quint64 frames;
    float fps;

    quint64 duration; // in milliseconds

    enum class PixFmt {
        YUV_420,  // 8/10/12/14/16-bit
        YUV_422,  // ^
        YUV_444,  // ^
        RGB24,    //  8-bit only
        RGB48LE,  // 16-bit only
        RGB48BE,  // ^
        BGR24,    //  8-bit only
        BGR48LE,  // 16-bit only
        BGR48BE,  // ^
        Unknown
    } pixfmt;

    quint8 depth;
};

#endif // AVINFO_HPP
