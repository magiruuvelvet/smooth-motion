#include "AVUtil.hpp"

#include <cstdio>
#include <cstring>
#include <algorithm>

#ifdef __cplusplus
extern "C" {
#endif
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/pixdesc.h>
#include <libavutil/pixfmt.h>
#include <libavutil/avutil.h>
#include <libavutil/mathematics.h>
#ifdef __cplusplus
}
#endif

AVUtil::AVUtil(const QString &v)
    : video(v)
{
    AVFormatContext *format_ctx = avformat_alloc_context();
    int initial_level = av_log_get_level();
    av_log_set_level(AV_LOG_ERROR); // make quiet

    int rc = avformat_open_input(&format_ctx, this->video.toUtf8().constData(), NULL, NULL);
    if (rc != 0) {
        // todo: report error "open input failed"
        return;
    }

    rc = avformat_find_stream_info(format_ctx, NULL);
    if (rc < 0) {
        avformat_close_input(&format_ctx);
        // todo: report error "no stream info found"
        return;
    }

    int v_stream_idx = av_find_best_stream(format_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);
    int a_stream_idx = av_find_best_stream(format_ctx, AVMEDIA_TYPE_AUDIO, -1, -1, NULL, 0);
    int s_stream_idx = av_find_best_stream(format_ctx, AVMEDIA_TYPE_SUBTITLE, -1, -1, NULL, 0);

    // check if streams exist and can be decoded
    int v_stream_exists = 1;
    int a_stream_exists = 1;
    int s_stream_exists = 1;

    if (v_stream_idx == AVERROR_STREAM_NOT_FOUND ||
        v_stream_idx == AVERROR_DECODER_NOT_FOUND)
    {
        v_stream_exists = 0;
    }
    if (a_stream_idx == AVERROR_STREAM_NOT_FOUND ||
        a_stream_idx == AVERROR_DECODER_NOT_FOUND)
    {
        a_stream_exists = 0;
    }
    if (s_stream_idx == AVERROR_STREAM_NOT_FOUND ||
        s_stream_idx == AVERROR_DECODER_NOT_FOUND)
    {
        s_stream_exists = 0;
    }

    int w = 0;
    int h = 0;
    int64_t duration = 0.0;    /* in milliseconds */
    unsigned long frames = 0;
    AVRational sar  = {0, 0};  /* sample aspect ratio */
    AVRational dar  = {0, 0};  /* display aspect ratio */
    float rate = 0.0;          /* average fps */
    AVRational rational_rate = {0, 0};

    if (v_stream_exists) {
        AVStream *v_stream = format_ctx->streams[v_stream_idx];

        AVRational ms_tb = {1, MS_PER_SEC};
        AVRational av_tb = {1, AV_TIME_BASE};

        int64_t v_duration = av_rescale_q(v_stream->duration,
                                          v_stream->time_base, ms_tb);
        int64_t c_duration = av_rescale_q(format_ctx->duration, av_tb, ms_tb);

        duration = v_duration;
        if (duration < 0 || almost_equal(v_duration, 0)) {
            /* fallback to the container duration if the video stream
             * doesnt report anything */
            duration = c_duration;
        }

        // < AVCodecContext *v_codec_ctx = format_ctx->streams[v_stream_idx]->codec; >
        // is deprecated and will stop working on modern ffmpeg version (including those who use the git version)
        // AVCodecParameters is the new AVCodecContext
        // just replace this code if your "ancient" ffmpeg version doesn't has AVCodecParameters yet ;)
        AVCodecParameters *v_codec_ctx = format_ctx->streams[v_stream_idx]->codecpar;

        w = v_codec_ctx->width;
        h = v_codec_ctx->height;

        rational_rate = format_ctx->streams[v_stream_idx]->avg_frame_rate;
        rate = rational_rate.num * 1.0 / rational_rate.den;

        /* calculate num of frames ourselves */
        frames = rate * duration / 1000.0;

        /* if sample aspect ratio is unknown assume it is 1:1 */
        sar = format_ctx->streams[v_stream_idx]->sample_aspect_ratio;
        if (sar.num == 0) {
            sar.num = 1;
            sar.den = 1;
        }

        /* the display aspect ratio can be calculated from the pixel aspect
         * ratio and sample aspect ratio: PAR * SAR = DAR.*/
        int dar_n = w * sar.num;
        int dar_d = h * sar.den;
        int g = gcd(dar_n, dar_d);  /* use gcd to reduce to simplest terms */

        dar.num = dar_n / g;
        dar.den = dar_d / g;

        /* get pixel format and bit depth */
        //const char *pix_fmt = av_get_pix_fmt_name((AVPixelFormat)format_ctx->streams[v_stream_idx]->parser->format);
        //printf("%s %d", pix_fmt, format_ctx->streams[v_stream_idx]->parser->format);
        //std::free((void*)pix_fmt);
        // todo, ffmpeg pls :(
        av_info.pixfmt = AVInfo::PixFmt::Unknown;
        av_info.depth = 0;
    }

    av_log_set_level(initial_level); // reset log level

    // close file and clean up
    avformat_close_input(&format_ctx);

    // init av_info object
    av_info.has_video = v_stream_exists;
    av_info.width = w;
    av_info.height = h;
    av_info.sar_n = sar.num;
    av_info.sar_d = sar.den;
    av_info.dar_n = dar.num;
    av_info.dar_d = dar.den;
    av_info.frames = frames;
    av_info.fps = rate;
    av_info.duration = duration;


    // finally set this object to valid
    this->valid = true;
}

AVUtil::~AVUtil()
{
    this->video.clear();
    this->valid = false;
}

const AVInfo &AVUtil::get_av_info() const
{
    return this->av_info;
}

const QByteArray AVUtil::get_av_info_as_text() const
{
    // get values for time string
    int x;
    int hrs, mins, secs;
    float duration = av_info.duration;
    x = duration / 1000.f;
    secs = x % 60;
    x /= 60;
    mins = x % 60;
    x /= 60;
    hrs = x % 24;

    QString dur;
    dur.sprintf("%02d:%02d:%02d", hrs, mins, secs);

    return QByteArray("Video information:")
         + "\n  Has video stream   \t: " + (av_info.has_video ? "yes" : "no")
         + "\n  Resolution         \t: " + QByteArray::number(av_info.width) + "x" + QByteArray::number(av_info.height) + ", "
                                  "SAR " + QByteArray::number(av_info.sar_n) + ":" + QByteArray::number(av_info.sar_d) + ", "
                                  "DAR " + QByteArray::number(av_info.dar_n) + ":" + QByteArray::number(av_info.dar_d)
         + "\n  Frame rate         \t: " + QByteArray::number(av_info.fps)
         + "\n  Num of Frames      \t: " + QByteArray::number(av_info.frames) + " WARNING: may be inaccurate for non-raw (aka. encoded) videos"
         + "\n  Duration            \t: " + dur.toUtf8()
         + "\n  Pixel format       \t: " + this->pixfmtStr(av_info.pixfmt).toUtf8()
         + "\n  Bit depth          \t: " + QByteArray::number(av_info.depth)
         + "\n"
         ;
}

const QString AVUtil::pixfmtStr(const AVInfo::PixFmt &pixfmt)
{
    switch (pixfmt)
    {
        case AVInfo::PixFmt::YUV_420: return "YUV 4:2:0"; break;
        case AVInfo::PixFmt::YUV_422: return "YUV 4:2:2"; break;
        case AVInfo::PixFmt::YUV_444: return "YUV 4:4:4"; break;
        case AVInfo::PixFmt::RGB24:   return "RGB24 (8*3) Little-Endian"; break;
        case AVInfo::PixFmt::RGB48LE: return "RGB48 (16*3) Little-Endian"; break;
        case AVInfo::PixFmt::RGB48BE: return "RGB48 (16*3) Big-Endian"; break;
        case AVInfo::PixFmt::BGR24:   return "BGR24 (8*3) Little-Endian"; break;
        case AVInfo::PixFmt::BGR48LE: return "BGR48 (16*3) Little-Endian"; break;
        case AVInfo::PixFmt::BGR48BE: return "BGR48 (16*3) Big-Endian"; break;
        default: return "<unknown>"; break;
    }
}
