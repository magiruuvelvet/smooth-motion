#ifndef AVUTIL_HPP
#define AVUTIL_HPP

#include "AVInfo.hpp"

#include <cmath>

#include <QString>
#include <QByteArray>

class AVUtil
{
public:
    AVUtil(const QString &video);
    ~AVUtil();

    // for easy validation check
    inline operator bool(void) {
        return valid;
    };

    // returns information required for the motion interpolation process
    const AVInfo &get_av_info() const;
    const QByteArray get_av_info_as_text() const;

    static const QString pixfmtStr(const AVInfo::PixFmt &pixfmt);

private:
    static inline int gcd(int a, int b) { return b == 0 ? a : gcd(b, a % b); }
    static inline int almost_equal(double a, double b) { return std::fabs(a - b) <= EPSILON; }

    static constexpr quint16 MS_PER_SEC = 1000;
    static constexpr double EPSILON = 1.0e-8;

    QString video;
    bool valid = false;
    AVInfo av_info;
};

#endif // AVUTIL_HPP
